<?php

namespace Drupal\comment_perm_search\Plugin\Search;

use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Database\StatementInterface;
use Drupal\node\Plugin\Search\NodeSearch as NodeSearchBase;

/**
 * Overrides NodeSearch plugin to invoke own node_update_index hook.
 */
class NodeSearch extends NodeSearchBase {

  /**
   * {@inheritdoc}
   */
  protected function prepareResults(StatementInterface $found) {
    $results = [];

    $node_storage = $this->entityTypeManager->getStorage('node');
    $node_render = $this->entityTypeManager->getViewBuilder('node');
    $keys = $this->keywords;

    foreach ($found as $item) {
      // Render the node.
      /** @var \Drupal\node\NodeInterface $node */
      $node = $node_storage->load($item->sid)->getTranslation($item->langcode);
      $build = $node_render->view($node, 'search_result', $item->langcode);

      /** @var \Drupal\node\NodeTypeInterface $type*/
      $type = $this->entityTypeManager->getStorage('node_type')->load($node->bundle());

      unset($build['#theme']);
      $build['#pre_render'][] = [$this, 'removeSubmittedInfo'];

      // Fetch comments for snippet.
      $rendered = $this->renderer->renderPlain($build);
      $this->addCacheableDependency(CacheableMetadata::createFromRenderArray($build));
      $rendered .= ' ' . $this->moduleHandler->invoke('comment_perm_search', 'node_update_index', [$node]);

      $extra = $this->moduleHandler->invokeAll('node_search_result', [$node]);

      $username = [
        '#theme' => 'username',
        '#account' => $node->getOwner(),
      ];

      $result = [
        'link' => $node->toUrl('canonical', ['absolute' => TRUE])->toString(),
        'type' => $type->label(),
        'title' => $node->label(),
        'node' => $node,
        'extra' => $extra,
        'score' => $item->calculated_score,
        'snippet' => search_excerpt($keys, $rendered, $item->langcode),
        'langcode' => $node->language()->getId(),
      ];

      $this->addCacheableDependency($node);

      // We have to separately add the node owner's cache tags because search
      // module doesn't use the rendering system, it does its own rendering
      // without taking cacheability metadata into account. So we have to do it
      // explicitly here.
      $this->addCacheableDependency($node->getOwner());

      if ($type->displaySubmitted()) {
        $result += [
          'user' => $this->renderer->renderPlain($username),
          'date' => $node->getChangedTime(),
        ];
      }

      $results[] = $result;

    }
    return $results;
  }

}
