<?php

namespace Drupal\comment_perm;

use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ServiceProviderBase;

/**
 * Comment permissions service provider.
 */
class CommentPermServiceProvider extends ServiceProviderBase {

  /**
   * {@inheritdoc}
   */
  public function register(ContainerBuilder $container) {
    $container->getDefinition('comment.link_builder')->setClass('Drupal\comment_perm\CommentLinkBuilder');
    $container->getDefinition('comment.manager')->setClass('Drupal\comment_perm\CommentManager');
  }

}
