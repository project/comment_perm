<?php

namespace Drupal\comment_perm;

use Drupal\comment\CommentManager as CommentManagerBase;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Url;
use Drupal\user\RoleInterface;
use Drupal\user\UserInterface;

/**
 * Overrides CommentManager service class.
 */
class CommentManager extends CommentManagerBase {

  /**
   * {@inheritdoc}
   */
  public function forbiddenMessage(EntityInterface $entity, $field_name) {
    if (!isset($this->authenticatedCanPostComments)) {
      // We only output a link if we are certain that users will get the
      // permission to post comments by logging in.
      $auth_role = $this->entityTypeManager->getStorage('user_role')
        ->load(RoleInterface::AUTHENTICATED_ID);

      if ($auth_role->hasPermission('post comments') || $auth_role->hasPermission("post {$entity->bundle()} comments")) {
        $this->authenticatedCanPostComments = TRUE;
      }
    }

    if ($this->authenticatedCanPostComments) {
      // We cannot use the redirect.destination service here because these links
      // sometimes appear on /node and taxonomy listing pages.
      if ($entity->get($field_name)->getFieldDefinition()->getSetting('form_location') == CommentItemInterface::FORM_SEPARATE_PAGE) {
        $comment_reply_parameters = [
          'entity_type' => $entity->getEntityTypeId(),
          'entity' => $entity->id(),
          'field_name' => $field_name,
        ];
        $destination = ['destination' => Url::fromRoute('comment.reply', $comment_reply_parameters, ['fragment' => 'comment-form'])->toString()];
      }
      else {
        $destination = ['destination' => $entity->toUrl('canonical', ['fragment' => 'comment-form'])->toString()];
      }

      if ($this->userConfig->get('register') != UserInterface::REGISTER_ADMINISTRATORS_ONLY) {
        // Users can register themselves.
        return $this->t('<a href=":login">Log in</a> or <a href=":register">register</a> to post comments', [
          ':login' => Url::fromRoute('user.login', [], ['query' => $destination])->toString(),
          ':register' => Url::fromRoute('user.register', [], ['query' => $destination])->toString(),
        ]);
      }
      else {
        // Only admins can add new users, no public registration.
        return $this->t('<a href=":login">Log in</a> to post comments', [
          ':login' => Url::fromRoute('user.login', [], ['query' => $destination])->toString(),
        ]);
      }
    }
    return '';
  }

}
