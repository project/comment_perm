<?php

namespace Drupal\comment_perm;

use Drupal\Core\Session\AccountInterface;

/**
 * Class CommentAccessTrait
 */
trait CommentAccessTrait {

  /**
   * Check if user has access to administer comments.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to administer comments, FALSE otherwise.
   */
  public function accessAdministerComment(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('administer comments') || $account->hasPermission("administer $comment_type comments")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user doesn't have access to administer comments.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user doesn't have administer comments permisison, FALSE otherwise.
   */
  public function noAccessAdministerComment(AccountInterface $account, $comment_type) {
    if (!$account->hasPermission('administer comments') && !$account->hasPermission("administer $comment_type comments")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user has access to administer comment type.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to administer comment type, FALSE otherwise.
   */
  public function accessAdministerCommentType(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('administer comment type') || $account->hasPermission("administer $comment_type comment type")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user has access to comments.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to view comments, FALSE otherwise.
   */
  public function accessComment(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('access comments') || $account->hasPermission("access $comment_type comments")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user has access to comments.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to view comments, FALSE otherwise.
   */
  public function accessPostComment(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('post comments') || $account->hasPermission("post $comment_type comments")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user has access to skip comment approval.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to skip comment approval, FALSE otherwise.
   */
  public function accessSkipCommentApproval(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('skip comment approval') || $account->hasPermission("skip $comment_type comment approval")) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Check if user has access to edit own comment.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   User account proxy.
   * @param $comment_type
   *   Comment entity type.
   *
   * @return bool
   *   TRUE user has access to edit own comment, FALSE otherwise.
   */
  public function accessEditOwnComment(AccountInterface $account, $comment_type) {
    if ($account->hasPermission('edit own comments') || $account->hasPermission("edit $comment_type own comments")) {
      return TRUE;
    }

    return FALSE;
  }

}
