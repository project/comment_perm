<?php

namespace Drupal\comment_perm\Access;

use Drupal\comment\CommentManagerInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\comment_perm\CommentAccessTrait;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Session\AccountProxyInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Comment types additional overrides for access checks.
 */
class CommentAccess implements ContainerInjectionInterface {

  use CommentAccessTrait;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The comment manager.
   *
   * @var \Drupal\comment\CommentManagerInterface
   */
  protected $commentManager;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * CommentAccess constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\comment\CommentManagerInterface $comment_manager
   *   The comment manager.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, CommentManagerInterface $comment_manager, AccountProxyInterface $current_user) {
    $this->entityTypeManager = $entity_type_manager;
    $this->commentManager = $comment_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('comment.manager'),
      $container->get('current_user')
    );
  }

  /**
   * Access check for the reply form.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity this comment belongs to.
   * @param string $field_name
   *   The field_name to which the comment belongs.
   * @param int $pid
   *   (optional) Some comments are replies to other comments. In those cases,
   *   $pid is the parent comment's comment ID. Defaults to NULL.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   An access result.
   */
  public function replyFormAccess(EntityInterface $entity, $field_name, $pid = NULL) {
    // Check if entity and field exists.
    $fields = $this->commentManager->getFields($entity->getEntityTypeId());
    if (empty($fields[$field_name])) {
      throw new NotFoundHttpException();
    }

    $account = $this->currentUser;
    $comment_type = $entity->get($field_name)->getSettings()['comment_type'];

    // Check if the user has the proper permissions.
    $access = AccessResult::allowedIf($this->accessPostComment($account, $comment_type));

    // If commenting is open on the entity.
    $status = $entity->{$field_name}->status;
    $access = $access->andIf(AccessResult::allowedIf($status == CommentItemInterface::OPEN)
      ->addCacheableDependency($entity))
      // And if user has access to the host entity.
      ->andIf(AccessResult::allowedIf($entity->access('view')));

    // $pid indicates that this is a reply to a comment.
    if ($pid) {
      // Check if the user has the proper permissions.
      $access = $access->andIf(AccessResult::allowedIf($this->accessComment($account, $comment_type)));

      // Load the parent comment.
      $comment = $this->entityTypeManager->getStorage('comment')->load($pid);
      // Check if the parent comment is published and belongs to the entity.
      $access = $access->andIf(AccessResult::allowedIf($comment && $comment->isPublished() && $comment->getCommentedEntityId() == $entity->id()));
      if ($comment) {
        $access->addCacheableDependency($comment);
      }
    }
    return $access;
  }

}
