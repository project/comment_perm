<?php

namespace Drupal\comment_perm;

use Drupal\comment\CommentFieldItemList as CommentFieldItemListBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;

/**
 * Overrides CommentFieldItemList class.
 */
class CommentFieldItemList extends CommentFieldItemListBase {

  use CommentAccessTrait;

  /**
   * {@inheritdoc}
   */
  public function access($operation = 'view', AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $account ?: \Drupal::currentUser();
    $comment_type = $this->getFieldDefinition()->getSettings()['comment_type'];

    if ($operation === 'edit') {
      // Only users with administer comments permission can edit the comment
      // status field.
      $result = AccessResult::allowedIf($this->accessAdministerComment($account, $comment_type));
      return $return_as_object ? $result : $result->isAllowed();
    }
    if ($operation === 'view') {
      // Only users with "post comments" or "access comments" permission can
      // view the field value. The formatter,
      // Drupal\comment\Plugin\Field\FieldFormatter\CommentDefaultFormatter,
      // takes care of showing the thread and form based on individual
      // permissions, so if a user only has ‘post comments’ access, only the
      // form will be shown and not the comments.
      $result = AccessResult::allowedIf($this->accessComment($account, $comment_type))
        ->orIf(AccessResult::allowedIf($this->accessPostComment($account, $comment_type)));
      return $return_as_object ? $result : $result->isAllowed();
    }
    return parent::access($operation, $account, $return_as_object);
  }

}
