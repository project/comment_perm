<?php

namespace Drupal\comment_perm\Entity;

use Drupal\comment\CommentInterface;
use Drupal\comment\Entity\Comment as CommentBase;
use Drupal\comment_perm\CommentAccessTrait;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Overrides Comment entity class.
 */
class Comment extends CommentBase {

  use CommentAccessTrait;

  /**
   * {@inheritdoc}
   */
  public function preSave(EntityStorageInterface $storage) {
    parent::preSave($storage);

    if (!$this->get('status')->value) {
      // Check if user has access to skip comment approval for a specific
      // comment type, before to post the comment.
      $skip_approval = $this->accessSkipCommentApproval($storage->currentUser(), $this->bundle());
      $status = $skip_approval ? CommentInterface::PUBLISHED : CommentInterface::NOT_PUBLISHED;
      $this->set('status',$status);
    }
  }

}
